<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8"/>
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>人事管理システム</title>
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- Bootstrap CSS（Cerulean） -->
  <link rel="stylesheet" href="/css/bootstrap.min.css">
  <!-- Bootstrap DataTables CSS -->
  <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/dt-1.10.20/datatables.min.css"/>

  <!-- 自作CSS -->
  <link rel="stylesheet" href="/css/common.css">

  <!-- JavaScript -->

</head>
<body>
  <!-- トップバー -->
  <?php include ('header.html'); ?>

  <div class="com-contents text-right">
    <a href="/employee_add.php"><button class="btn btn-primary">新規作成</button></a>
  </div>

  <?php
    //-----------------
    // 社員リスト取得
    //-----------------
    // MySQLへ接続
    $con = mysql_connect('hrms-lamp_mysql_1', 'root', 'root') or die('error(connect)');

    // データベースの選択
    mysql_select_db('test', $con) or die('error(select_db)');

    // 文字コード設定
    mysql_set_charset('utf8');

    // クエリー生成
    $query = 'select e.code, e.last_name, e.first_name, d.code as department_code, d.name as department_name from employee as e ';
    $query .= 'join department as d ';
    $query .= 'on e.department_code = d.code';

    // クエリー実行
    $employee_list = mysql_query($query, $con);
  ?>
  <div class="com-contents">
    <table id="employee_list" class="table table-striped table-bordered">
      <!-- ヘッダー -->
      <thead>
        <tr>
          <th>社員コード</th>
          <th>社員名（性）</th>
          <th>社員名（名）</th>
          <th>所属部署コード</th>
          <th>所属部署名</th>
        </tr>
      </thead>

      <!-- データ -->
      <tbody>
        <?php
          while ($row = mysql_fetch_array($employee_list)) {
        ?>
        <tr>
          <td><?php echo $row['code'];?></td>
          <td><?php echo $row['last_name'];?></td>
          <td><?php echo $row['first_name'];?></td>
          <td><?php echo $row['department_code'];?></td>
          <td><?php echo $row['department_name'];?></td>
        </tr>
        <?php
          }
        ?>
      </tbody>
    </table>
  </div>
  <?php
    // MySQLへの接続を切断
    mysql_close($con);
  ?>

  <!-- footer_js -->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script type="text/javascript" src="https://cdn.datatables.net/v/dt/dt-1.10.20/datatables.min.js"></script>

  <script>
    // DataTable化
    $(document).ready(function($){ 
      $("#employee_list").DataTable(); 
    });
  </script>
  <!-- footer_js end -->
</body>
</html>