<?php
  // POSTデータ取得
  $last_name = "";
  if (!empty($_POST['last_name']))
  {
    $last_name = $_POST['last_name'];
  }
  $first_name = "";
  if (!empty($_POST['first_name']))
  {
    $first_name = $_POST['first_name'];
  }
  $department_code = "";
  if (!empty($_POST['department_code']))
  {
    $department_code = $_POST['department_code'];
  }
  //echo '<p class="com-contents">$department_code='.$department_code.'</p>';

  $post_flag = false;
  if (!empty($last_name) || !empty($first_name) || !empty($department_code))
  {
    $post_flag = true;
  }

  if ($post_flag)
  {
    //-----------------
    // データ登録
    //-----------------
    // MySQLへ接続
    $con = mysql_connect('hrms-lamp_mysql_1', 'root', 'root') or die('error(connect)');

    // データベースの選択
    mysql_select_db('test', $con) or die('error(select_db)');

    // 文字コード設定
    mysql_set_charset('utf8');

    // 社員コード取得用クエリー生成
    $query_code = 'select max(code) as max_code from employee';

    // 社員コード取得用クエリー実行
    $result_code = mysql_query($query_code, $con);

    $new_code = 1;
    if (!empty($result_code))
    {
      $row = mysql_fetch_array($result_code);
      if (!empty($row['max_code']))
      {
        $new_code = $row['max_code'] + 1;
      }
    }
    //echo '<p class="com-contents">$new_code='.$new_code.'</p>';

    // 新規社員登録用クエリー生成
    $query_insert = 'INSERT INTO employee(code, last_name, first_name, department_code) VALUES('.
    $query_insert .= $new_code.',';
    $query_insert .= '"'.$last_name.'",';
    $query_insert .= '"'.$first_name.'",';
    $query_insert .= $department_code;
    $query_insert .= ')';

    //echo '<p class="com-contents">$query_insert='.$query_insert.'</p>';

    // クエリー実行
    $result = mysql_query($query_insert, $con);

    // MySQLへの接続を切断
    mysql_close($con);

    // 結果OKの場合は社員リストに移動
    if ($result) {
      header('Location: ./index.php');
      exit;
    }
    else
    {
      $err_detail = '('.$new_code.',';
      $err_detail .= '"'.$last_name.'",';
      $err_detail .= '"'.$first_name.'",';
      $err_detail .= $department_code.')';
    }
  }
?>

<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8"/>
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>社員追加 - 人事管理システム</title>
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- Bootstrap CSS（Cerulean） -->
  <link rel="stylesheet" href="/css/bootstrap.min.css">
  <!-- Bootstrap DataTables CSS -->
  <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/dt-1.10.20/datatables.min.css"/>

  <!-- 自作CSS -->
  <link rel="stylesheet" href="/css/common.css">

  <!-- JavaScript -->

</head>

<body>
  <!-- トップバー -->
  <?php include ('header.html'); ?>

  <div class="com-contents">
    <h1>社員追加</h1>
  </div>

  <?php
    // データ登録処理を実行した場合
    if ($post_flag)
    {
      // 結果OKの場合は社員リストに移動
      if ($result) {
        echo '<label class="com-contents error">'.$err_detail.'</label>';
      }
    }

    //-----------------
    // 部署リスト取得
    //-----------------
    // MySQLへ接続
    $con = mysql_connect('hrms-lamp_mysql_1', 'root', 'root') or die('error(connect)');

    // データベースの選択
    mysql_select_db('test', $con) or die('error(select_db)');

    // クエリー生成
    $query = 'select * from department';

    // クエリー実行
    $department_list = mysql_query($query, $con);
  ?>

  <form id="form-department" method="post">
    <div class="com-contents">
      <div class="container">
        <div class="row form-group">
          <div class="col-md-3 col-xs-3">
            <label for="last_name" class="col-form-label">社員名（性）</label>
          </div>
          <div class="col-md-9 col-xs-9">
            <input type="text" class="form-control" name="last_name" size="10" value="<?php echo $last_name; ?>">
          </div>
        </div>

        <div class="row form-group">
          <div class="col-md-3 col-xs-3">
            <label for="first_name" class="col-form-label">社員名（名）</label>
          </div>
          <div class="col-md-9 col-xs-9">
            <input type="text" class="form-control" name="first_name" size="10" value="<?php echo $first_name; ?>">
          </div>
        </div>

        <div class="row form-group">
          <div class="col-md-3 col-xs-3">
            <label for="department" class="col-form-label">所属部署</label>
          </div>
          <div class="col-md-9 col-xs-9">
            <select class="form-control" name="department_code">
            <?php
              while ($row = mysql_fetch_array($department_list)) {

                $selected = "";
                if (strcmp($row['code'], $department_code) == 0)
                {
                  $selected = "selected='selected'";
                }
            ?>
              <option value="<?php echo $row['code'];?>" <?php echo $selected;?>><?php echo $row['name'];?></option>
            <?php
              }
            ?>
            </select>
          </div>
        </div>

        <div class="row form-group text-right">
          <div class="col-md-12 col-xs-12">
            <input type="submit" id="department_add" value="追加" class="btn btn-primary">
          </div>
        </div>
      </div>
    </div>
  </form>

  <!-- footer_js -->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/jquery.validate.min.js"></script>
  <script>
    $(document).ready(function($){ 
      
      // バリデーション
      const formDepartment = $("#form-department");
      formDepartment.validate({
        rules : {
          last_name: {
            required: true
          },
          first_name: {
            required: true
          }
        },
        messages: {
          last_name:{
              required: "必須項目です。（10文字以内）"
          },
          first_name:{
              required: "必須項目です。（10文字以内）"
          },
        }
      });

      // 追加ボタン押下
      $('#department_add').submit(function() {
        console.log("#department_add clicked.");

        if (formDepartment.validate().form()) {
            console.log("OK");
            //$('#department_add').submit();
            return true;
        } else {
            console.log("NG");
            return false;
        }
      });

    });
  </script>
  <!-- footer_js end -->
</body>
</html>